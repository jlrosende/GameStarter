cmake_minimum_required(VERSION 3.6)
project(gamestarter)

set(CMAKE_CXX_STANDARD 17)
set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++1y -Wall")

set (Boost_USE_STATIC_LIBS OFF) # enable dynamic linking
set (Boost_USE_MULTITHREAD ON)  # enable multithreading
find_package (Boost COMPONENTS REQUIRED system chrono filesystem thread)
IF (Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIR})
endif()

find_package(OpenGL REQUIRED) # finds OpenGL, GLU and X11
find_package(PkgConfig REQUIRED)
pkg_search_module(GLFW REQUIRED glfw3)
find_package(GLEW REQUIRED)
find_package(glm REQUIRED)
find_package(Freetype REQUIRED)
find_package(yaml-cpp REQUIRED)

if (GLFW_FOUND)
    include_directories(${GLFW_INCLUDE_DIRS})
endif()

if (GLEW_FOUND)
    include_directories(${GLEW_INCLUDE_DIRS})
    add_definitions(-DGLEW_STATIC)
endif()

#Encontrando y linkeando glm
if (GLM_FOUND)
    include_directories(${GLM_INCLUDE_DIRS})
endif()

include_directories(${FREETYPE_INCLUDE_DIRS})

if (YAML_CPP_FOUND)
    include_directories(${YAML_CPP_INCLUDE_DIRS})
endif()

include_directories(/usr/include/SOIL)
link_directories(/usr/lib/SOIL)

include_directories(/usr/include/assimp)
link_directories(/usr/lib/assimp)

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

set(SOURCE_FILES main.cpp RenderEngine/Window.cpp GameCore/Game.cpp RenderEngine/Shader.cpp GameCore/ResourceManager.cpp Objects/Object.cpp Objects/Mesh.cpp GameCore/GameMap.cpp Objects/Model.cpp Objects/Cameras/Camera.cpp Objects/Lights/Light.cpp Objects/Font.cpp Objects/Lights/PointLight.cpp Objects/Lights/PointLight.h Objects/Lights/SpotLight.cpp Objects/Lights/SpotLight.h Objects/Lights/DirLight.cpp Objects/Lights/DirLight.h Objects/Terreins/Terrain.cpp Objects/Terreins/Terrain.h RenderEngine/Renderer.cpp RenderEngine/Renderer.h GameCore/Loaders/AnimationLoader.cpp GameCore/Loaders/AnimationLoader.h GameCore/Loaders/FontLoader.cpp GameCore/Loaders/FontLoader.h GameCore/Loaders/ShaderLoader.cpp GameCore/Loaders/ShaderLoader.h GameCore/Loaders/ModelLoader.cpp GameCore/Loaders/ModelLoader.h GameCore/Loaders/GameMapLoader.cpp GameCore/Loaders/GameMapLoader.h Objects/Animation.cpp Objects/Animation.h Objects/Joint.cpp Objects/Joint.h Objects/JointTransform.cpp Objects/JointTransform.h Objects/KeyFrame.cpp Objects/KeyFrame.h RenderEngine/Animator.cpp RenderEngine/Animator.h FisicsEngine/Constants.h)
add_executable(gamestarter ${SOURCE_FILES})

# linking "glfw" and not "glfw3"
# assumes that glfw was built with BUILD_SHARED_LIBS to ON
target_link_libraries(gamestarter ${OPENGL_LIBRARY} ${GLFW_STATIC_LIBRARIES} ${GLEW_LIBRARIES} ${FREETYPE_LIBRARIES} ${YAML_CPP_LIBRARIES} ${Boost_FILESYSTEM_LIBRARY}
        ${Boost_SYSTEM_LIBRARY} ${Boost_LIBRARIES} SOIL assimp Threads::Threads)
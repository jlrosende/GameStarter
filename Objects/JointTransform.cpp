//
// Created by rosende95 on 11/12/17.
//

#include "JointTransform.h"

JointTransform::JointTransform(glm::vec3 position, glm::quat rotation) : position(position), rotation(rotation) { }

glm::mat4 JointTransform::getLocalTransform() {
    glm::mat4 matrix;
    matrix = glm::translate(matrix, position);
    matrix *= glm::toMat4(rotation);
    return matrix;
}

JointTransform JointTransform::interpolate(JointTransform frameA, JointTransform frameB, float progression) {
    glm::vec3 pos = glm::mix(frameA.position, frameB.position, progression);
    glm::quat rot = glm::mix(frameA.rotation, frameB.rotation, progression);
    return JointTransform(pos, rot);
}



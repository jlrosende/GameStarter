//
// Created by rosende95 on 16/06/17.
//

#include <atomic>
#include "Model.h"

unsigned int TextureFromFile(const char *path, const std::string &directory, bool gamma) {
    std::string filename = std::string(path);
    filename = directory + '/' + filename;

    std::replace( filename.begin(), filename.end(), '\\', '/');

    boost::filesystem::path p(filename);
    boost::filesystem::path full_path = boost::filesystem::complete(p); // complete == absolute

    GLuint textureID;
    glGenTextures(1, &textureID);

    int width, height, channels;
    unsigned char *data = SOIL_load_image(full_path.c_str(), &width, &height, &channels, SOIL_LOAD_AUTO);

    if (data) {
        GLenum format;
        if (channels == 1)
            format = GL_RED;
        else if (channels == 3)
            format = GL_RGB;
        else if (channels == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        SOIL_free_image_data(data);
    } else {
        std::cerr << "Texture failed to load at path: " << full_path << std::endl;
        SOIL_free_image_data(data);
    }

    return textureID;
}

Model::Model(std::string const &path, bool gamma) : gammaCorrection(gamma), rootJoint(nullptr) {
    loadModel(path);
}

// TODO Borrar este metodo
void printNode(int depth, aiNode *node) {
    for (int i = 0; i < depth; ++i) {
        std::cout << "\t";
    }
    std::cout << depth <<" " << node->mName.data << std::endl;
    for (int j = 0; j < node->mNumChildren; ++j) {
        printNode(++depth, node->mChildren[j]);
    }
}

void Model::loadModel(std::string const &path) {

    // read file via ASSIMP
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(
            path,
            aiProcess_GenSmoothNormals |
            aiProcess_LimitBoneWeights |
            aiProcess_JoinIdenticalVertices |
            aiProcess_Triangulate |
            aiProcess_FlipUVs |
            aiProcess_CalcTangentSpace
    );

    // check for errors
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) { // if is Not Zero
        std::cerr << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }
    // retrieve the directory path of the filepath
    directory = path.substr(0, path.find_last_of('/'));

    // process ASSIMP's root node recursively
    processNode(scene->mRootNode, scene);
    //printNode(0, scene->mRootNode);
}

void Model::processNode(aiNode *node, const aiScene *scene) {
    //std::cout << "Node: " << node->mName.data << std::endl;
    // process each mesh located at the current node
    //std::vector<Bone> bones;
    for (unsigned int i = 0; i < node->mNumMeshes; i++) {
        // the node object only contains indices to index the actual objects in the scene.
        // the scene contains all the data, node is just to keep stuff organized (like relations between nodes).
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        meshes.push_back(processMesh(mesh, scene));
        //bones = processBone(mesh, scene);
    }
    // after we've processed all of the meshes (if any) we then recursively process each of the children nodes
    for(unsigned int i = 0; i < node->mNumChildren; i++) {
        processNode(node->mChildren[i], scene);
    }
}

Mesh Model::processMesh(aiMesh *mesh, const aiScene *scene) {
    // data to fill
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    std::vector<Material> materials;
    // Walk through each of the mesh's vertices
    for(unsigned int i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex;
        glm::vec3 vector; // we declare a placeholder vector since assimp uses its own vector class that doesn't directly convert to glm's vec3 class so we transfer the data to this placeholder glm::vec3 first.
        // positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        // normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.Normal = vector;
        // texture coordinates
        if(mesh->mTextureCoords[0]) // does the mesh contain texture coordinates?
        {
            glm::vec2 vec;
            // a vertex can contain up to 8 different texture coordinates. We thus make the assumption that we won't
            // use models where a vertex can have multiple texture coordinates so we always take the first set (0).
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoords = vec;
        } else {
            vertex.TexCoords = glm::vec2(0.0f, 0.0f);
        }

        if (mesh->HasTangentsAndBitangents()) {
            // tangent
            vector.x = mesh->mTangents[i].x;
            vector.y = mesh->mTangents[i].y;
            vector.z = mesh->mTangents[i].z;
            vertex.Tangent = vector;

            // bitangent
            vector.x = mesh->mBitangents[i].x;
            vector.y = mesh->mBitangents[i].y;
            vector.z = mesh->mBitangents[i].z;
            vertex.Bitangent = vector;
        }

        vertices.push_back(vertex);
    }

    // now wak through each of the mesh's faces (a face is a mesh its triangle) and retrieve the corresponding vertex indices.
    for(unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        // retrieve all indices of the face and store them in the indices vector
        for(unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }
    // process materials
    if(mesh->mMaterialIndex >= 0)
    {
        aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

        aiString name;
        aiColor3D diffuse;
        aiColor3D specular;
        aiColor3D ambient;
        float opacity;
        float shininess;

        material->Get(AI_MATKEY_NAME, name);
        material->Get(AI_MATKEY_COLOR_DIFFUSE, diffuse);
        material->Get(AI_MATKEY_COLOR_SPECULAR, specular);
        material->Get(AI_MATKEY_COLOR_AMBIENT, ambient);
        material->Get(AI_MATKEY_SHININESS, opacity);
        material->Get(AI_MATKEY_SHININESS, shininess);

        Material mat;

        mat.name = name.data;

        mat.diffuse.r = diffuse.r;
        mat.diffuse.g = diffuse.g;
        mat.diffuse.b = diffuse.b;

        mat.specular.r = specular.r;
        mat.specular.g = specular.g;
        mat.specular.b = specular.b;

        mat.ambient.r = ambient.r;
        mat.ambient.g = ambient.g;
        mat.ambient.b = ambient.b;

        mat.opacity = opacity;

        mat.shininess = shininess;

        materials.push_back(mat);

        //std::cout << "Name: " << name.data << std::endl;
        //std::cout << "Diffuse: (" << diffuse.r << ", " << diffuse.g << ", " << diffuse.b << ")" << std::endl;
        //std::cout << "Specular: (" << specular.r << ", " << specular.g << ", " << specular.b << ")" << std::endl;
        //std::cout << "Ambient: (" << ambient.r << ", " << ambient.g << ", " << ambient.b << ")" << std::endl;
        //std::cout << "Opacity: " << opacity << std::endl;
        //std::cout << "Shiniess: " << shininess << std::endl;

        // we assume a convention for sampler names in the shaders. Each diffuse texture should be named
        // as 'texture_diffuseN' where N is a sequential number ranging from 1 to MAX_SAMPLER_NUMBER.
        // Same applies to other texture as the following list summarizes:
        // diffuse: texture_diffuseN
        // specular: texture_specularN
        // normal: texture_normalN

        // 1. diffuse maps
        std::vector<Texture> diffuseMaps = loadMaterialTextures(material, aiTextureType_DIFFUSE, "texture_diffuse");
        textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());
        // 2. specular maDragon_ground_color.jpgps
        std::vector<Texture> specularMaps = loadMaterialTextures(material, aiTextureType_SPECULAR, "texture_specular");
        textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());
        // 3. normal maps
        std::vector<Texture> normalMaps = loadMaterialTextures(material, aiTextureType_HEIGHT, "texture_normal");
        textures.insert(textures.end(), normalMaps.begin(), normalMaps.end());
        // 4. height maps
        std::vector<Texture> heightMaps = loadMaterialTextures(material, aiTextureType_AMBIENT, "texture_height");
        textures.insert(textures.end(), heightMaps.begin(), heightMaps.end());
    }


    // return a mesh object created from the extracted mesh data
    return Mesh(vertices, indices, textures, materials);
}

/*
std::vector<Bone> Model::processBone(aiMesh *mesh, const aiScene *scene) {
    std::vector<Bone> bones;
    // Process Bones
    for (unsigned int i = 0; i < mesh->mNumBones; i++) {

        glm::mat4 offsetMatrix;
        offsetMatrix[0][0] = mesh->mBones[i]->mOffsetMatrix.a1;
        offsetMatrix[1][0] = mesh->mBones[i]->mOffsetMatrix.b1;
        offsetMatrix[2][0] = mesh->mBones[i]->mOffsetMatrix.c1;
        offsetMatrix[3][0] = mesh->mBones[i]->mOffsetMatrix.d1;

        offsetMatrix[0][1] = mesh->mBones[i]->mOffsetMatrix.a2;
        offsetMatrix[1][1] = mesh->mBones[i]->mOffsetMatrix.b2;
        offsetMatrix[2][1] = mesh->mBones[i]->mOffsetMatrix.c2;
        offsetMatrix[3][1] = mesh->mBones[i]->mOffsetMatrix.d2;

        offsetMatrix[0][2] = mesh->mBones[i]->mOffsetMatrix.a3;
        offsetMatrix[1][2] = mesh->mBones[i]->mOffsetMatrix.b3;
        offsetMatrix[2][2] = mesh->mBones[i]->mOffsetMatrix.c3;
        offsetMatrix[3][2] = mesh->mBones[i]->mOffsetMatrix.d3;

        offsetMatrix[0][3] = mesh->mBones[i]->mOffsetMatrix.a4;
        offsetMatrix[1][3] = mesh->mBones[i]->mOffsetMatrix.b4;
        offsetMatrix[2][3] = mesh->mBones[i]->mOffsetMatrix.c4;
        offsetMatrix[3][3] = mesh->mBones[i]->mOffsetMatrix.d4;

        //std::cout << mesh->mBones[i]->mNumWeights << std::endl;
        std::vector<VertexWeight> vertexWeights;
        for (unsigned int j = 0; j < mesh->mBones[i]->mNumWeights; j++) {
            VertexWeight vertexWeight;
            vertexWeight.id = mesh->mBones[i]->mWeights[j].mVertexId;
            vertexWeight.weight = mesh->mBones[i]->mWeights[j].mWeight;
            vertexWeights.push_back(vertexWeight);
        }
        Bone bone(mesh->mBones[i]->mName.data, offsetMatrix, vertexWeights);
        //std::cout << "\tBone: " << mesh->mBones[i]->mName.data << std::endl;
        bones.push_back(bone);
    }

    return bones;
}
*/
std::vector<Texture> Model::loadMaterialTextures(aiMaterial *mat, aiTextureType type, std::string typeName) {
    std::vector<Texture> textures;
    for(unsigned int i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        // check if texture was loaded before and if so, continue to next iteration: skip loading a new texture
        bool skip = false;
        for(unsigned int j = 0; j < textures_loaded.size(); j++)
        {
            if(std::strcmp(textures_loaded[j].path.C_Str(), str.C_Str()) == 0)
            {
                textures.push_back(textures_loaded[j]);
                skip = true; // a texture with the same filepath has already been loaded, continue to next one. (optimization)
                break;
            }
        }
        if(!skip)
        {   // if texture hasn't been loaded already, load it
            Texture texture;
            texture.id = TextureFromFile(str.C_Str(), this->directory);
            texture.type = typeName;
            texture.path = str;
            textures.push_back(texture);
            textures_loaded.push_back(texture);  // store it as texture loaded for entire model, to ensure we won't unnecesery load duplicate textures.
        }
    }
    return textures;
}

Joint Model::getRootJoint() {
    return *this->rootJoint;
}

void Model::render(Shader *shader) {
    //std::cout << "\tModel: " << &model << std::endl;
    for (unsigned int i = 0; i < this->meshes.size(); i++)
        this->meshes[i].render(shader);
}


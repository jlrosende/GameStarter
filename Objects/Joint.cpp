//
// Created by rosende95 on 11/12/17.
//

#include "Joint.h"

Joint::Joint(int id, std::string name, glm::mat4 bindLocalTransform)
        : id(id), name(name), localBindTransform(bindLocalTransform),
          animatedTransform(glm::mat4()), inverseBindTransform(glm::mat4()) { }

void Joint::addChild(Joint child)  {
    this->children.push_back(child);
}

glm::mat4 Joint::getAnimatedTransform() {
    return animatedTransform;
}

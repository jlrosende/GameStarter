//
// Created by rosende on 10/06/17.
//

#ifndef GAMESTARTER_MESH_H
#define GAMESTARTER_MESH_H

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/types.h>

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>

#include "../RenderEngine/Shader.h"

struct Join {
    std::string name;
    float weight;
};
struct Vertex {
    // Posicion
    glm::vec3 Position;
    // Normal
    glm::vec3 Normal;
    // Coordenadas Textura
    glm::vec2 TexCoords;
    // Tangente
    glm::vec3 Tangent;
    // Bitangente
    glm::vec3 Bitangent;
    // Bones
    Join boneWeight[4];
};

struct Texture {
    unsigned int id;
    std::string type;
    aiString path;
    float shinniess;
};

struct Material {
    std::string name;
    glm::vec3 diffuse;
    glm::vec3 specular;
    glm::vec3 ambient;
    float opacity;
    float shininess;
};

class Mesh {

    GLuint VBO, EBO;

    void setupMesh();

public:

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<Texture> textures;
    std::vector<Material> materials;

    GLuint VAO;

    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures, std::vector<Material> materials);

    void render(Shader *shader);

};


#endif //GAMESTARTER_MESH_H

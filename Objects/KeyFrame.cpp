//
// Created by rosende95 on 11/12/17.
//

#include "KeyFrame.h"

KeyFrame::KeyFrame(float timeStamp, std::map<std::string, JointTransform> pose) : timeStamp(timeStamp), pose(pose) { }

float KeyFrame::getTimeStamp() {
    return timeStamp;
}

std::map<std::string, JointTransform> KeyFrame::
getJointKeyFrames() {
    return pose;
}

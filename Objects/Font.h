//
// Created by rosende95 on 28/06/17.
//

#ifndef GAMESTARTER_FONT_H
#define GAMESTARTER_FONT_H

#include <map>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <ft2build.h>
#include "freetype/freetype.h"

#include "../RenderEngine/Shader.h"

struct Character {
    GLuint TextureID;   // ID handle of the glyph texture
    glm::ivec2 Size;    // Size of glyph
    glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
    GLuint Advance;    // Horizontal offset to advance to next glyph
};

class Font {

    GLuint VAO, VBO;
    Shader shader;

public:

    std::map<GLchar, Character> Characters;

    Font();
    ~Font();

    void Draw(Shader shader, std::string text, GLfloat x, GLfloat y, GLfloat scale, glm::vec3 color);

};


#endif //GAMESTARTER_FONT_H

//
// Created by rosende on 8/06/17.
//

#include "Object.h"

Object::Object() {

}

Object::Object(Model model, Shader shader, glm::vec3 position, glm::vec3 lookAt, glm::vec3 size) : model(model), shader(shader), position(position), lookAt(lookAt), size(size), solid(false) {

}

Object::~Object() {

}

glm::vec3 Object::getPosition() {
    return position;
}

void Object::setPosition(glm::vec3 position) {
    this->position = position;
}

glm::vec3 Object::getSize()  {
    return size;
}

void Object::setSize(glm::vec3 size) {
    this->size = size;
}

bool Object::isSolid() {
    return this->solid;
}

void Object::setSolid(bool solid) {
    this->solid = solid;
}

void Object::setModel(Model model) {
    this->model = model;
}

Model *Object::getModel() {
    return &model;
}

void Object::setShader(Shader shader) {
    this->shader = shader;
}

Shader *Object::getShader() {
    return &shader;
}

glm::mat4 Object::getTransformationMatrix() {
    glm::mat4 mat;
    mat = glm::translate(mat, this->getPosition());
    mat = glm::rotate(mat, lookAt.x, glm::vec3(1,0,0));
    mat = glm::rotate(mat, lookAt.y, glm::vec3(0,1,0));
    mat = glm::rotate(mat, lookAt.z, glm::vec3(0,0,1));
    mat = glm::scale(mat, this->getSize());
    return mat;
}

void Object::rotate(glm::vec3 rotate) {
    this->lookAt = rotate;
}

void Object::update(float ft) {

}

void Object::render() {
    shader.Use();
    shader.SetMatrix4("model", this->getTransformationMatrix());
    model.render(&this->shader);
}



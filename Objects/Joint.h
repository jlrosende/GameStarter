//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_JOINT_H
#define GAMESTARTER_JOINT_H

#include <iostream>
#include <vector>
#include <glm/glm.hpp>

class Joint {
public:
    int id;
    std::string name;
    std::vector<Joint> children;
    glm::mat4 animatedTransform;
    glm::mat4 localBindTransform;
    glm::mat4 inverseBindTransform;

public:
    Joint(int id, std::string name, glm::mat4 bindLocalTransform);

    void addChild(Joint child);

    glm::mat4 getAnimatedTransform();

    void setAnimationTransform(glm::mat4 animationTransform) {
        this->animatedTransform = animationTransform;
    }

    glm::mat4 getInverseBindTransform() {
        return inverseBindTransform;
    }


    void calcInverseBindTransform(glm::mat4 parentBindTransform) {
        glm::mat4 bindTransform = parentBindTransform * localBindTransform;
        bindTransform = glm::inverse(inverseBindTransform);
        for (Joint child : children) {
            child.calcInverseBindTransform(bindTransform);
        }
    }
};


#endif //GAMESTARTER_JOINT_H

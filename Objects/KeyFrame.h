//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_KEYFRAME_H
#define GAMESTARTER_KEYFRAME_H


#include <map>
#include "JointTransform.h"

class KeyFrame {
    float timeStamp;
    std::map<std::string, JointTransform> pose;

public:
    KeyFrame(float timeStamp, std::map<std::string, JointTransform> pose);

    float getTimeStamp();

    std::map<std::string, JointTransform> getJointKeyFrames();

};


#endif //GAMESTARTER_KEYFRAME_H

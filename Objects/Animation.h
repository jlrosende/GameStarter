//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_ANIMATION_H
#define GAMESTARTER_ANIMATION_H


#include <vector>
#include <iostream>
#include <assimp/scene.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include "KeyFrame.h"

class Animation {

    std::vector<std::string> nodeNames;

    float length;
    std::vector<KeyFrame*> keyFrames;

    void loadAnimation(std::string const &path);

public:
    Animation() = default;
    explicit Animation(std::string const &path);

    float getLength();

    std::vector<KeyFrame*> getKeyFrames();
};


#endif //GAMESTARTER_ANIMATION_H

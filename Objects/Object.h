//
// Created by rosende on 8/06/17.
//

#ifndef GAMESTARTER_OBJECT_H
#define GAMESTARTER_OBJECT_H

#include <GL/glew.h>
#include <glm/glm.hpp>

#include <iostream>
#include <string>
#include <vector>

#include "Model.h"

class Object {

protected:
    glm::vec3 position;
    glm::vec3 size;
    glm::vec3 lookAt;

    bool solid;

    Model model;
    Shader shader;

public:
    Object();
    Object(Model model, Shader shader, glm::vec3 position = glm::vec3(0.0f), glm::vec3 lookAt = glm::vec3(0.0f, 0.0f, 90.0f), glm::vec3 size = glm::vec3(1.0f));
    ~Object();

    glm::vec3 getPosition();
    void setPosition(glm::vec3 position);

    glm::vec3 getSize();
    void setSize(glm::vec3 size);

    bool isSolid();
    void setSolid(bool solid);

    Model *getModel();
    void setModel(Model model);

    Shader *getShader();
    void setShader(Shader shader);

    void rotate(glm::vec3 rotate);

    glm::mat4 getTransformationMatrix();

    virtual void update(float ft);
    virtual void render();

};


#endif //GAMESTARTER_OBJECT_H

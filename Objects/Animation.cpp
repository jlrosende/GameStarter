//
// Created by rosende95 on 11/12/17.
//



#include "Animation.h"

Animation::Animation(std::string const &path){
    loadAnimation(path);
}

float Animation::getLength() {
    return length;
}

std::vector<KeyFrame*> Animation::getKeyFrames() {
    return keyFrames;
}

void Animation::loadAnimation(std::string const &path) {

    // read file via ASSIMP
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(
            path,
            aiProcess_GenSmoothNormals |
            aiProcess_LimitBoneWeights |
            aiProcess_JoinIdenticalVertices |
            aiProcess_Triangulate |
            aiProcess_FlipUVs |
            aiProcess_CalcTangentSpace
    );

    // check for errors
    if(!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) { // if is Not Zero
        std::cerr << "ERROR::ASSIMP:: " << importer.GetErrorString() << std::endl;
        return;
    }
    std::string directory;
    // retrieve the directory path of the filepath
    directory = path.substr(0, path.find_last_of('/'));

    float duration;

    for (unsigned int i = 0; i < scene->mNumAnimations; ++i) {
        duration = (float) scene->mAnimations[i]->mDuration;
        scene->mAnimations[i]->mName.data;
        //scene->mAnimations[i]->mChannels[0]->;
    }


    std::vector<KeyFrame*> keyFrames;

    std::map<std::string, JointTransform> jointTransforms;

    glm::vec3 pos;
    glm::quat rotation;
    JointTransform jointTransform();

}

//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_JOINTTRANSFORM_H
#define GAMESTARTER_JOINTTRANSFORM_H


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>

class JointTransform {
    glm::vec3 position;
    glm::quat rotation;

public:
    JointTransform(glm::vec3 position, glm::quat rotation);

    glm::mat4 getLocalTransform();

    static JointTransform interpolate(JointTransform transform, JointTransform jointTransform, float progression);
};


#endif //GAMESTARTER_JOINTTRANSFORM_H

//
// Created by rosende95 on 10/07/17.
//

#include "DirLight.h"

DirLight::DirLight(glm::vec3 position, glm::vec3 direction, glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular, glm::vec3 size)
        : Light(position, ambient, diffuse, specular, size), direction(direction) { }


glm::vec3 DirLight::getDirection() {
    return direction;
}

void DirLight::setDirection(glm::vec3 direction) {
    this->direction = direction;
}

void DirLight::render(int nLight) {
    Light::render();
    std::string s("dirLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    shader.SetVector3f((s + "position").c_str(), this->getPosition());
    shader.SetVector3f((s + "direction").c_str(), this->getDirection());
    shader.SetVector3f((s + "ambient").c_str(), this->getAmbient());
    shader.SetVector3f((s + "diffuse").c_str(), this->getDiffuse());
    shader.SetVector3f((s + "specular").c_str(), this->getSpecular());
}





//
// Created by rosende95 on 10/07/17.
//

#include "PointLight.h"

PointLight::PointLight(glm::vec3 position, glm::vec3 ambient, glm::vec3 diffuse,
                       glm::vec3 specular, float constant,
                       float linear, float quadratic, glm::vec3 size)
        : Light(position, ambient, diffuse, specular, size),
          constant(constant), linear(linear), quadratic(quadratic) { }

float PointLight::getConstant() {
    return constant;
}

void PointLight::setConstant(float constant) {
    this->constant = constant;
}

float PointLight::getLinear() {
    return linear;
}

void PointLight::setLinear(float linear) {
    this->linear = linear;
}

float PointLight::getQuadratic() {
    return quadratic;
}

void PointLight::setQuadratic(float quadratic) {
    this->quadratic = quadratic;
}

void PointLight::render(int nLight) {
    Light::render();
    std::string s("pointLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    shader.SetVector3f((s + "position").c_str(), this->getPosition());
    shader.SetVector3f((s + "ambient").c_str(), this->getAmbient());
    shader.SetVector3f((s + "diffuse").c_str(), this->getDiffuse());
    shader.SetVector3f((s + "specular").c_str(), this->getSpecular());
    shader.SetFloat((s + "constant").c_str(), this->getConstant());
    shader.SetFloat((s + "linear").c_str(), this->getLinear());
    shader.SetFloat((s + "quadratic").c_str(), this->getQuadratic());
}



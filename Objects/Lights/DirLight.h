//
// Created by rosende95 on 10/07/17.
//

#ifndef GAMESTARTER_DIRLIGHT_H
#define GAMESTARTER_DIRLIGHT_H

#include "Light.h"

class DirLight : virtual public Light {

protected:
    glm::vec3 direction;

public:
    DirLight(glm::vec3 position, glm::vec3 direction = glm::vec3(0.0f), glm::vec3 ambient = glm::vec3(1.0f),
             glm::vec3 diffuse = glm::vec3(1.0f), glm::vec3 specular = glm::vec3(1.0f), glm::vec3 size = glm::vec3(1.0f));

    glm::vec3 getDirection();
    void setDirection(glm::vec3 direction);

    void render(int nLight);
};


#endif //GAMESTARTER_DIRLIGHT_H

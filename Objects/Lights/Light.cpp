//
// Created by rosende95 on 28/06/17.
//

#include "Light.h"

Light::Light(glm::vec3 position, glm::vec3 ambient, glm::vec3 diffuse,
             glm::vec3 specular, glm::vec3 size, Model model, Shader shader) : Object(model, shader, position, glm::vec3(0.0f),
        size), ambient(ambient), diffuse(diffuse), specular(specular) { }

glm::vec3 Light::getColor() {
    return (ambient+diffuse+specular);
}

glm::vec3 Light::getAmbient() {
    return ambient;
}

void Light::setAmbient(glm::vec3 ambient) {
    this->ambient = ambient;
}

glm::vec3 Light::getDiffuse() {
    return diffuse;
}

void Light::setDiffuse(glm::vec3 diffuse) {
    this->diffuse = diffuse;
}

glm::vec3 Light::getSpecular() {
    return specular;
}

void Light::setSpecular(glm::vec3 specular) {
    this->specular = specular;
}

void Light::render() {
    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("model", this->getTransformationMatrix());
    ResourceManager::GetShader("light").SetVector3f("lightColor", (this->getColor()));
    Shader shader1 = ResourceManager::GetShader("light");
    this->getModel()->render(&shader1);
    shader.Use();
    shader.SetVector3f("lightPos", this->getPosition());
}





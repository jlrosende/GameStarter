//
// Created by rosende95 on 28/06/17.
//

#ifndef GAMESTARTER_LIGHT_H
#define GAMESTARTER_LIGHT_H

#include <glm/glm.hpp>

#include "../Object.h"
#include "../../GameCore/ResourceManager.h"


class Light : public Object {


protected:

    glm::vec3 ambient;
    glm::vec3 diffuse;
    glm::vec3 specular;

public:

    Light(glm::vec3 position, glm::vec3 ambient = glm::vec3(1.0f), glm::vec3 diffuse = glm::vec3(1.0f),
          glm::vec3 specular = glm::vec3(1.0f), glm::vec3 size = glm::vec3(1.0f), Model model = ResourceManager::GetModel("light"),
          Shader shader = ResourceManager::GetShader("model"));

    glm::vec3 getAmbient();
    void setAmbient(glm::vec3 ambient);

    glm::vec3 getDiffuse() ;
    void setDiffuse(glm::vec3 diffuse);

    glm::vec3 getSpecular();
    void setSpecular(glm::vec3 specular);

    glm::vec3 getColor();

    void render();

};


#endif //GAMESTARTER_LIGHT_H

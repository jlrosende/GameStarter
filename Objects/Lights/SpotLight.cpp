//
// Created by rosende95 on 10/07/17.
//

#include "SpotLight.h"

SpotLight::SpotLight(glm::vec3 position, glm::vec3 direction, glm::vec3 ambient,
                     glm::vec3 diffuse, glm::vec3 specular, float constant,
                     float linear, float quadratic, float cutOff,
                     float outerCutOff, glm::vec3 size)
        : cutOff(cutOff), outerCutOff(outerCutOff), DirLight(position, direction, ambient, diffuse, specular, size),
          PointLight(position, ambient, diffuse, specular, constant, linear, quadratic, size),
          Light(position, ambient, diffuse, specular, size) { }

float SpotLight::getCutOff() {
    return cutOff;
}

void SpotLight::setCutOff(float cutOff) {
    this->cutOff = cutOff;
}

float SpotLight::getOuterCutOff() {
    return outerCutOff;
}

void SpotLight::setOuterCutOff(float outerCutOff) {
    this->outerCutOff = outerCutOff;
}

void SpotLight::render(int nLight) {
    Light::render();
    std::string s("spotLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    shader.SetVector3f((s + "position").c_str(), this->getPosition());
    shader.SetVector3f((s + "direction").c_str(), this->getDirection());
    shader.SetVector3f((s + "ambient").c_str(), this->getAmbient());
    shader.SetVector3f((s + "diffuse").c_str(), this->getDiffuse());
    shader.SetVector3f((s + "specular").c_str(), this->getSpecular());
    shader.SetFloat((s + "constant").c_str(), this->getConstant());
    shader.SetFloat((s + "linear").c_str(), this->getLinear());
    shader.SetFloat((s + "quadratic").c_str(), this->getQuadratic());
    shader.SetFloat((s + "cutOff").c_str(), this->getCutOff());
    shader.SetFloat((s + "outerCutOff").c_str(), this->getOuterCutOff());
}



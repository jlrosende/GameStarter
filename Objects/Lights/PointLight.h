//
// Created by rosende95 on 10/07/17.
//

#ifndef GAMESTARTER_POINTLIGHT_H
#define GAMESTARTER_POINTLIGHT_H

#include "Light.h"

class PointLight : virtual public Light {

protected:
    float constant;
    float linear;
    float quadratic;

public:
    PointLight(glm::vec3 position, glm::vec3 ambient = glm::vec3(1.0f), glm::vec3 diffuse = glm::vec3(1.0f),
               glm::vec3 specular = glm::vec3(1.0f), float constant = 1.0f,
               float linear = 0.09f, float quadratic = 0.032f, glm::vec3 size = glm::vec3(0.0f));

    float getConstant();
    void setConstant(float constant);

    float getLinear();
    void setLinear(float linear);

    float getQuadratic();
    void setQuadratic(float quadratic);

    void render(int nLight);

};


#endif //GAMESTARTER_POINTLIGHT_H

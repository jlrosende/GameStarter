//
// Created by rosende95 on 10/07/17.
//

#ifndef GAMESTARTER_SPOTLIGHT_H
#define GAMESTARTER_SPOTLIGHT_H

#include "Light.h"
#include "DirLight.h"
#include "PointLight.h"

class SpotLight : public DirLight, public PointLight {

    float cutOff;
    float outerCutOff;

public:
    SpotLight(glm::vec3 position, glm::vec3 direction = glm::vec3(0.0f), glm::vec3 ambient = glm::vec3(1.0f),
              glm::vec3 diffuse = glm::vec3(1.0f), glm::vec3 specular = glm::vec3(1.0f), float constant = 1.0f,
              float linear = 0.09f, float quadratic = 0.032f, float cutOff = glm::cos(glm::radians(12.5f)),
              float outerCutOff =  glm::cos(glm::radians(15.0f)), glm::vec3 size = glm::vec3(1.0f));
    ~SpotLight();

    float getCutOff();
    void setCutOff(float cutOff);

    float getOuterCutOff();
    void setOuterCutOff(float outerCutOff);

    void render(int nLight);

};


#endif //GAMESTARTER_SPOTLIGHT_H

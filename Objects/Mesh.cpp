//
// Created by rosende on 10/06/17.
//

#include "Mesh.h"

Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures, std::vector<Material> materials) {
    this->vertices = vertices;
    this->indices = indices;
    this->textures = textures;
    this->materials = materials;

    this->setupMesh();
}

void Mesh::setupMesh() {
    // Creamos buffers/arrays
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO);
    // cargamos los datos en el vertex buffer
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), &vertices[0], GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices[0], GL_STATIC_DRAW);

    // Ponemos el atributos a los vertices
    // vertex Position
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)0);
    // vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Normal));
    // vertex Texture Coord
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, TexCoords));
    // vertex tangent
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Tangent));
    // vertex bitangent
    glEnableVertexAttribArray(4);
    glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

    /*
    // Bone id
    glEnableVertexAttribArray(5);
    glVertexAttribPointer(5, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));

    // Bone weight
    glEnableVertexAttribArray(6);
    glVertexAttribPointer(6, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)offsetof(Vertex, Bitangent));
     */
}

void Mesh::render(Shader *shader) {
    unsigned int diffuseNr  = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr   = 1;
    unsigned int heightNr   = 1;
    if (this->textures.size() > 0) {
        glUniform1i(glGetUniformLocation(shader->ID, "HadTextures"), 1);
    } else {
        glUniform1i(glGetUniformLocation(shader->ID, "HadTextures"), 0);
    }
    for(unsigned int i = 0; i < this->textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i); // activar cada textura antes de unirlas
        // asigno un nombre para cada textura
        std::stringstream ss;
        std::string number;
        std::string name = this->textures[i].type.data();
        if (name == "texture_diffuse") {
            ss << diffuseNr++;
        } else if (name == "texture_specular") {
            ss << specularNr++;
        } else if (name == "texture_normal") {
            ss << normalNr++;
        } else if (name == "texture_height") {
            ss << heightNr++;
        }
        number = ss.str();
        // paso las texturas al shader
        //std::cout << ("material." + name + number) << std::endl;
        glUniform1i(glGetUniformLocation(shader->ID, ("material." + name + number).c_str()), i);
        // Linco las texturas segun su id
        glBindTexture(GL_TEXTURE_2D, this->textures[i].id);
    }

    for (unsigned int i = 0; i < this->materials.size(); i++) {
        shader->SetVector3f("material.diffuse_color", this->materials[i].diffuse);
        shader->SetVector3f("material.specular_color", this->materials[i].specular);
        shader->SetVector3f("material.ambient_color", this->materials[i].ambient);
        shader->SetFloat("material.opacity", this->materials[i].opacity);
        shader->SetFloat("material.shininess", this->materials[i].shininess);
    }

    // pintar maya
    glBindVertexArray(this->VAO);
    glDrawElements(GL_TRIANGLES, this->indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // always good practice to set everything back to defaults once configured.
    glActiveTexture(GL_TEXTURE0);
}

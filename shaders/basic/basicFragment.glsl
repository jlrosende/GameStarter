#version 450 core
out vec4 FragColor;

in vec2 TexCoords;

struct Material {
    sampler2D texture_diffuse1;
    vec3 diffuse_color;
    sampler2D texture_specular1;
    vec3 specular_color;
    sampler2D texture_normal1;
    sampler2D texture_height1;
    float opacity;
    float shininess;
};

uniform Material material;

uniform bool HadTextures;


void main()
{
    if (HadTextures) {
        FragColor = texture(material.texture_diffuse1, TexCoords) * vec4(1.0f);
    } else {
        FragColor = vec4(material.diffuse_color, 1.0f);
    }
}
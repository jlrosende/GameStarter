#version 450 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

struct Material {
    sampler2D texture_diffuse1;
    //vec3 diffuse_color;
    sampler2D texture_specular1;
    //vec3 diffuse_specular;
    sampler2D texture_normal1;
    sampler2D texture_height1;
    float shininess;
    float opacity;
};

uniform Material material;

void main()
{
     // obtain normal from normal map in range [0,1]
    vec3 normal = texture(material.texture_normal1, fs_in.TexCoords).rgb;
    // transform normal vector to range [-1,1]
    normal = normalize(normal * 2.0 - 1.0);  // this normal is in tangent space
    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);

    // get diffuse color
    vec3 color = texture(material.texture_diffuse1, fs_in.TexCoords).rgb;

    // ambient
    vec3 ambient = 0.1 * color;

    // diffuse
    vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
    float diff = max(dot(lightDir, normal), 0.0);
    vec3 diffuse = diff * color;

    // specular
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), 256.0);

    vec3 specular = texture(material.texture_specular1, fs_in.TexCoords).rgb * spec;


    vec3 result = ambient + diffuse + specular;

    // Gamma correction
    float gamma = 2.2;
    result.rgb = pow(result.rgb, vec3(1.0/gamma));

    FragColor = vec4(result, 1.0);
}

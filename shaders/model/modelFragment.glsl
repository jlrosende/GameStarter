#version 450 core
out vec4 FragColor;

in VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} fs_in;

in vec3 Normal;

struct Material {
    sampler2D texture_diffuse1;
    vec3 diffuse_color;
    sampler2D texture_specular1;
    vec3 specular_color;
    sampler2D texture_normal1;
    sampler2D texture_height1;
    float opacity;
    float shininess;
};

struct DirLight {
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct PointLight {
    vec3 position;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;

    float constant;
    float linear;
    float quadratic;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

uniform vec3 lightPos;
uniform vec3 viewPos;

#define NR_POINT_LIGHTS 48

uniform DirLight dirLights[NR_POINT_LIGHTS];
uniform int numDirLights;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform int numPointLights;
uniform SpotLight spotLights[NR_POINT_LIGHTS];
uniform int numSpotLights;
uniform Material material;

uniform bool HadTextures;

// function prototypes
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    vec3 norm;
    if (HadTextures) {
        // obtain normal from normal map in range [0,1]
        norm = texture(material.texture_normal1, fs_in.TexCoords).rgb;
        // transform normal vector to range [-1,1]
    } else {
        norm = Normal;
    }

    norm = normalize(norm * 2.0 - 1.0);  // this normal is in tangent space
    vec3 viewDir = normalize(fs_in.TangentViewPos - fs_in.TangentFragPos);

/*
    // Todas las luces
    vec3 result = CalcDirLight(dirLights[0], norm, viewDir);
    result += CalcPointLight(pointLights[0], norm, fs_in.FragPos, viewDir);
    result += CalcSpotLight(spotLights[0], norm, fs_in.FragPos, viewDir);
*/
    vec3 result;
    // phase 1: directional lighting
    for(int i = 0; i < numDirLights; i++)
        result = CalcDirLight(dirLights[i], norm, viewDir);

    // phase 2: point lights
    for(int i = 0; i < numPointLights; i++)
        result += CalcPointLight(pointLights[i], norm, fs_in.FragPos, viewDir);

    // phase 3: spot light
    for(int i = 0; i < numSpotLights; i++)
        result += CalcSpotLight(spotLights[i], norm, fs_in.FragPos, viewDir);

    // Gamma correction
    float gamma = 2.2;
    result.rgb = pow(result.rgb, vec3(1.0/gamma));

    FragColor = vec4(result, 1.0);
}
// calculates the color when using a directional light.
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // combine results
    vec3 ambient, diffuse, specular;
    if (HadTextures) {
        ambient = light.ambient * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        specular = light.specular * spec * vec3(texture(material.texture_specular1, fs_in.TexCoords));
    } else {
        ambient = light.ambient * material.diffuse_color;
        diffuse = light.diffuse * diff * material.diffuse_color;
        specular = light.specular * spec * material.specular_color;
    }
    return (ambient + diffuse + specular);
}

// calculates the color when using a point light.
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // combine results
    vec3 ambient, diffuse, specular;
    if (HadTextures) {
        ambient = light.ambient * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        specular = light.specular * spec * vec3(texture(material.texture_specular1, fs_in.TexCoords));
    } else {
        ambient = light.ambient * material.diffuse_color;
        diffuse = light.diffuse * diff * material.diffuse_color;
        specular = light.specular * spec * material.specular_color;
    }
    return (ambient + diffuse + specular);
}

// calculates the color when using a spot light.
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(fs_in.TangentLightPos - fs_in.TangentFragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float spec = pow(max(dot(normal, halfwayDir), 0.0), material.shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // combine results
    vec3 ambient, diffuse, specular;
    if (HadTextures) {
        ambient = light.ambient * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        diffuse = light.diffuse * diff * vec3(texture(material.texture_diffuse1, fs_in.TexCoords));
        specular = light.specular * spec * vec3(texture(material.texture_specular1, fs_in.TexCoords));
    } else {
        ambient = light.ambient * material.diffuse_color;
        diffuse = light.diffuse * diff * material.diffuse_color;
        specular = light.specular * spec * material.specular_color;
    }
    return (ambient + diffuse + specular);
}
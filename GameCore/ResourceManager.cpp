//
// Created by rosende on 25/02/17.
//

#include "ResourceManager.h"


// Instantiate static variables
std::map<std::string, Shader> ResourceManager::Shaders;
std::map<std::string, Model> ResourceManager::Models;
std::map<std::string, Font> ResourceManager::Fonts;
std::map<std::string, Animation> ResourceManager::Animations;

Shader ResourceManager::LoadShader(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile, std::string name) {
    Shaders[name] = ShaderLoader::LoadShader(vShaderFile, fShaderFile, gShaderFile, name);
    std::cout << "\t[OK] Shader: " << name << " load suscefully"  << std::endl;
    return Shaders[name];
}

Shader ResourceManager::GetShader(std::string name) {
    return Shaders[name];
}

Model ResourceManager::LoadModel(std::string const &modelFile, std::string name) {
    Model model(modelFile);
    Models[name] = model;
    std::cout << "\t[OK] Model: " << name << " load suscefully"  << std::endl;
    return Models[name];
}

Model ResourceManager::GetModel(std::string name) {
    return Models[name];
}


Font ResourceManager::LoadFont(std::string fontFile, std::string name) {
    Fonts[name] = FontLoader::LoadFont(fontFile);
    std::cout << "\t[OK] Font: " << name << " load suscefully"  << std::endl;
    return Fonts[name];
}

Font ResourceManager::GetFont(std::string name) {
    return Fonts[name];
}


Animation ResourceManager::LoadAnimation(std::string const &animationFile, std::string name) {
    Animation animation(animationFile);
    Animations[name] = animation;
    std::cout << "\t[OK] Animation: " << name << " load suscefully"  << std::endl;
    return Animations[name];
}

Animation ResourceManager::GetAnimation(std::string name) {
    return Animations[name];
}

void ResourceManager::Clear() {
    std::cout << "[ResourceManager] clear" << std::endl;
    // (Properly) delete all shaders
    for (auto iter : Shaders) {
        glDeleteProgram(iter.second.ID);
    }

    Shaders.clear();
    std::cout << "\t [Ok] Shades clear" << std::endl;

    Models.clear();
    std::cout << "\t [Ok] Models clear" << std::endl;

    Fonts.clear();
    std::cout << "\t [Ok] Fonts clear" << std::endl;


}

std::vector<Shader> ResourceManager::GetShaderList() {
    std::vector<Shader> shaderList;
    for (auto it=Shaders.begin(); it!=Shaders.end(); it++)
        shaderList.push_back(it->second);
    return shaderList;
}

std::vector<Model> ResourceManager::GetModelList() {
    std::vector<Model> modelList;
    for (auto it=Models.begin(); it!=Models.end(); it++)
        modelList.push_back(it->second);
    return modelList;
}

std::vector<Font> ResourceManager::GetFontList() {
    std::vector<Font> fontList;
    for (auto it=Fonts.begin(); it!=Fonts.end(); it++)
        fontList.push_back(it->second);
    return fontList;
}

std::vector<Animation> ResourceManager::GetAnimationList() {
    std::vector<Animation> animationList;
    for (auto it=Animations.begin(); it!=Animations.end(); it++)
        animationList.push_back(it->second);
    return animationList;
}

std::vector<std::string> ResourceManager::GetModelNamesList() {
    std::vector<std::string> names;
    for (auto it=Models.begin(); it!=Models.end(); it++)
        names.push_back(it->first);
    return names;
}







//
// Created by rosende on 25/02/17.
//

#ifndef GAMESTARTER_GAME_H
#define GAMESTARTER_GAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <yaml-cpp/yaml.h>

#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/thread.hpp>

#include <iostream>
#include <future>

#include "../RenderEngine/Window.h"
#include "ResourceManager.h"
#include "GameMap.h"
#include "../Objects/Cameras/Camera.h"
#include "../RenderEngine/Renderer.h"

enum GameState {
    GAME_ACTIVE,
    GAME_MENU
};

/** @defgroup group1 The First Group
 *  This is the first group
 */
class Game {

    Window *_window;

    int nbFrames;
    int lastnbFrames;

    static float deltaTime;
    float lastFrame;

    YAML::Node resourcesConfig;
    YAML::Node windowConfig;

public:

    static GameState state;

    GameMap *gameMap;
    Renderer *renderer;

    //! Constructor
    /**
     * Constructor base
     */
    Game();
    ~Game();

    void init();
    void gameLoop();
    void render();
    void update();

    void loadBasicConfiguration();

    void processInput();

    static float getFrameTimeSeconds();
};


#endif //GAMESTARTER_GAME_H

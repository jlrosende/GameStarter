//
// Created by rosende on 13/06/17.
//

#include "GameMap.h"


GameMap::GameMap() {
    this->init();
}

GameMap::~GameMap() = default;

void GameMap::init() {
    Object *m0 = new Object(ResourceManager::GetModel("budy"), ResourceManager::GetShader("model"), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(glm::radians(0.0f), 0.0f, 0.0f), glm::vec3(0.5));
    objects.push_back(m0);

    Object *m1 = new Object(ResourceManager::GetModel("walk"), ResourceManager::GetShader("model"), glm::vec3(5.0f, 0.0f, 0.0f), glm::vec3(0.0f), glm::vec3(0.05));
    objects.push_back(m1);

    Object *m2 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(-5.0f, 0.0f, 0.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m2);

    Object *m3 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(0.0f, 0.0f, -5.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m3);

    Object *m4 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(5.0f, 0.0f, -5.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m4);

    Object *m5 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(-5.0f, 0.0f, -5.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m5);

    Object *m6 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(5.0f, 0.0f, -10.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m6);

    Object *m7 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(.0f, 0.0f, -10.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m7);

    Object *m8 = new Object(ResourceManager::GetModel("nanosuit"), ResourceManager::GetShader("model"), glm::vec3(-5.0f, 0.0f, -10.0f), glm::vec3(0.0f), glm::vec3(0.5));
    objects.push_back(m8);

    DirLight *dl = new DirLight(glm::vec3(0.0f, 10.0f, -5.0f), glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.05f), glm::vec3(0.4f), glm::vec3(0.5f), glm::vec3(0.5f));
    dirLights.push_back(dl);

    PointLight *pl = new PointLight(glm::vec3(10.0f), glm::vec3(0.05f), glm::vec3(0.8f), glm::vec3(1.0f), 1.0f, 0.09f, 0.032f, glm::vec3(0.5f));
    pointLights.push_back(pl);

    SpotLight *sl = new SpotLight(glm::vec3(0.0f, 5.0f, 5.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f), glm::vec3(1.0f), glm::vec3(1.0f), 1.0f, 0.09f, 0.032f, glm::cos(glm::radians(12.5f)),glm::cos(glm::radians(15.0f)),  glm::vec3(0.5f));
    spotLights.push_back(sl);

    Camera *c0 = new Camera(glm::vec3(0.0f, 5.0f, 3.0f));
    cameras.push_back(c0);

    Camera *c1 = new Camera(glm::vec3(10.0f), glm::vec3(0.0f, 1.0f, 0.0f), (glm::vec3(0.0f) - glm::vec3(1.0f)));
    cameras.push_back(c1);

    activeCamera = 0;

}

void GameMap::setActiveCamera(int n)  {
    this->activeCamera = n;
}

Camera* GameMap::getActiveCamera() {
    return cameras[activeCamera];
}

void GameMap::setShader(Shader shader) {
    this->shader = shader;
}

Shader GameMap::getShader() {
    return shader;
}

void GameMap::render(Window *window) {
    // Configurar los shaders
    glm::mat4 projection = glm::perspective(glm::radians(this->getActiveCamera()->Zoom), window->getAspectRatio(), 0.1f, 100.0f);
    glm::mat4 view = this->getActiveCamera()->GetViewMatrix();

    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("projection", projection);
    ResourceManager::GetShader("light").SetMatrix4("view", view);

    if (!this->dirLights.empty()) {
        for (unsigned int i = 0; i < this->dirLights.size(); i++) {
            //render(gameMap->dirLights[i]);
            this->dirLights[i]->render(i);
        }
    }

    if (!this->pointLights.empty()) {
        for (unsigned int i = 0; i < this->pointLights.size(); i++) {
            //render(gameMap->pointLights[i]);
            this->pointLights[i]->render(i);
        }
    }

    if (!this->spotLights.empty()) {
        for (unsigned int i = 0; i < this->spotLights.size(); i++) {
            //render(gameMap->spotLights[i]);
            this->spotLights[i]->render(i);
        }
    }

    shader.Use();
    //ResourceManager::GetShader("model").SetVector3f("lightPos", lights[0]->getPosition());
    shader.SetMatrix4("projection", projection);
    shader.SetMatrix4("view", view);

    shader.SetInteger("numDirLights", (GLint) this->dirLights.size());
    shader.SetInteger("numPointLights", (GLint) this->pointLights.size());
    shader.SetInteger("numSpotLights", (GLint) this->spotLights.size());

    shader.SetVector3f("viewPos", this->getActiveCamera()->getPosition());

    for (auto object : this->objects) {
        object->setShader(shader);
        object->render();
    }
}

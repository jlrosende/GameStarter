//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_ANIMATIONLOADER_H
#define GAMESTARTER_ANIMATIONLOADER_H


#include "../../Objects/Animation.h"

class AnimationLoader {
public:
    static Animation LoadAnimation(std::string path);

};


#endif //GAMESTARTER_ANIMATIONLOADER_H

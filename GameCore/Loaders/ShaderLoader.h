//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_SHADERLOADER_H
#define GAMESTARTER_SHADERLOADER_H


#include "../../RenderEngine/Shader.h"

class ShaderLoader {
public:
    /**
     * Lee los archivos de los Shaders y crea un shader para poder utilizarlo en el engine
     * @param vShaderFile
     * @param fShaderFile
     * @param gShaderFile
     * @param name
     * @return Shader
     */
    static Shader LoadShader(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile, std::string name);
};


#endif //GAMESTARTER_SHADERLOADER_H

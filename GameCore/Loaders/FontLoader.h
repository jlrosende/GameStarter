//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_FONTLOADER_H
#define GAMESTARTER_FONTLOADER_H


#include "../../Objects/Font.h"

class FontLoader {
public:
    static Font LoadFont(std::string path);
};


#endif //GAMESTARTER_FONTLOADER_H

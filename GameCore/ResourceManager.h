//
// Created by rosende on 25/02/17.
//

#ifndef GAMESTARTER_RESOURCEMANAGER_H
#define GAMESTARTER_RESOURCEMANAGER_H

#include <glm/glm.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <map>

#include "Loaders/ShaderLoader.h"
#include "Loaders/FontLoader.h"
#include "Loaders/AnimationLoader.h"

#include "../RenderEngine/Shader.h"
#include "../Objects/Model.h"
#include "../Objects/Font.h"
#include "../Objects/Animation.h"

class ResourceManager {

public:
    // Resource storage
    static std::map<std::string, Shader> Shaders;
    static std::map<std::string, Model> Models;
    static std::map<std::string, Font> Fonts;
    static std::map<std::string, Animation> Animations;

    // Loads (and generates) a shader program from file loading vertex, fragment (and geometry) shader's source code. If gShaderFile is not nullptr, it also loads a geometry shader
    /**
     * Crea un Shader a traves de los distintos ficheros, lo guarda asignandole un nombre
     * @param vShaderFile
     * @param fShaderFile
     * @param gShaderFile
     * @param name
     * @return Shader
     */
    static Shader LoadShader(const GLchar *vShaderFile, const GLchar *fShaderFile, const GLchar *gShaderFile, std::string name);

    /**
     * Carga un modelo a traves de un fichero y lo guarda asignandole un nombre
     * @param modelFile
     * @param name
     * @return Model
     */
    static Model LoadModel(std::string const &modelFile, std::string name);

    /**
     * Carga una fuente a traves de un fichero y la guarda asignandole un nombre
     * @param fontFile
     * @param name
     * @return Font
     */
    static Font LoadFont(std::string fontFile, std::string name);

    /**
     * Carga una animacion a traves de un fichero y la guarda asignandole un nombre
     * @param animationFile
     * @param name
     * @return Animation
     */
    static Animation LoadAnimation(std::string const &animationFile, std::string name);

    /**
     * Devuelve un Shader guardado
     * @param name
     * @return Shader
     */
    static Shader GetShader(std::string name);

    /**
     * Devuelve un Model guardado
     * @param name
     * @return Model
     */
    static Model GetModel(std::string name);

    /**
     * Devuelve un Font guardado
     * @param name
     * @return Font
     */
    static Font GetFont(std::string name);

    /**
     * Devuelve un Animation guardado
     * @param name
     * @return Animation
     */
    static Animation GetAnimation(std::string name);

    /**
     * Devuelve una lista de Shader guardados
     * @return std::vector<Shader>
     */
    static std::vector<Shader> GetShaderList();

    /**
     * Devuelve una lista de Model guardados
     * @return std::vector<Model>
     */
    static std::vector<Model> GetModelList();

    /**
     * Devuelve una lista de Font guardados
     * @return std::vector<Font>
     */
    static std::vector<Font> GetFontList();

    /**
     * Devuelve una lista de Animatin guardados
     * @return std::vector<Animation>
     */
    static std::vector<Animation> GetAnimationList();

    static std::vector<std::string> GetModelNamesList();

    /**
     * Borra todos los recursos y vacia las listas
     */
    static void Clear();

};


#endif //GAMESTARTER_RESOURCEMANAGER_H

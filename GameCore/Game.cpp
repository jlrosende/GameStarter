//
// Created by rosende on 25/02/17.
//


#include "Game.h"

// settings
int SCR_WIDTH = 1280;
int SCR_HEIGHT = 720;

Camera *camera;
glm::vec3 lightPos(-5, 10, 5);

float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

GameState Game::state = GAME_ACTIVE;
float  Game::deltaTime = 0.0f;

// shader
int s = 0, arrayShaderLenght = 2;
std::string arrayShader[] = {"basic", "model"};

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        if (glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED) {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            Game::state = GAME_MENU;
        } else {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            Game::state = GAME_ACTIVE;
        }
    }

    if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
        s++;
    }
}

static void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    glViewport(0, 0, width, height);
}

static void mouse_callback(GLFWwindow* window, double xpos, double ypos){
    if (Game::state == GAME_ACTIVE) {
        if (firstMouse)
        {
            lastX = (float) xpos;
            lastY = (float) ypos;
            firstMouse = false;
        }

        float xoffset = (float) xpos - lastX;
        float yoffset = lastY - (float) ypos; // reversed since y-coordinates go from bottom to top

        lastX = (float) xpos;
        lastY = (float) ypos;

        camera->ProcessMouseMovement(xoffset, yoffset);
    } else if (Game::state == GAME_MENU) {

    }
}

static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    if (Game::state == GAME_ACTIVE) {
        camera->ProcessMouseScroll((float) yoffset);
    } else if (Game::state == GAME_MENU) {

    }
}


Game::Game() {

    std::cout << "[GAME]--> Load libraries" << std::endl;
    /* Initialize the library */

    if (!glfwInit()) {
        std::cout << "[ERROR] No se pudo inicial el glfw" << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "\t[OK] Init GLFW" << std::endl;
    }

    loadBasicConfiguration();

    if (windowConfig["window"]["windowMode"].as<int>() == 0) {
        _window = new Window("Test");
    } else {
        _window = new Window("Test", windowConfig["window"]["resolution"]["width"].as<int>(), windowConfig["window"]["resolution"]["height"].as<int>());
    }

    glfwSetKeyCallback(_window->getWindow(), key_callback);
    glfwSetFramebufferSizeCallback(_window->getWindow(), framebuffer_size_callback);
    glfwSetCursorPosCallback(_window->getWindow(), mouse_callback);
    glfwSetScrollCallback(_window->getWindow(), scroll_callback);

    // tell GLFW to capture our mouse
    //glfwSetInputMode(_window->getWindow(), GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    glewExperimental = GL_FALSE;
    GLenum error = glGetError();
    if (error != GL_NO_ERROR) {
        std::cout << "\t[ERROR] OpenGL Error: " << error << std::endl;
    }
    GLenum glewinit = glewInit();
    if (glewinit != GLEW_OK) {
        std::cout << "\t[ERROR] Glew not okay! " << glewinit << std::endl;
        exit(EXIT_FAILURE);
    } else {
        std::cout << "\t[OK] Glew okay! Status: " << glewinit << std::endl;
    }


    // Borra luego
    /*
    int nMonitors;
    GLFWmonitor **monitors = glfwGetMonitors(&nMonitors);
    for (int i = 0; i < nMonitors; i++) {
        int nModes;
        const GLFWvidmode *modes = glfwGetVideoModes(monitors[i], &nModes);
        for (int j = 0; j < nModes; j++) {
            std::cout << "Monitor nº: " << i << " Mode: " << j << std::endl;
            std::cout << "Resolution: " << modes[j].width << "x" << modes[j].height << std::endl;
            std::cout << "Refresh Rate: " << modes[j].refreshRate << std::endl;
            std::cout << "----------------------" << std::endl;
        }
    }
    */
}

Game::~Game() {
    glfwTerminate();
    std::cout << "--> End Game" << std::endl;
    exit(EXIT_SUCCESS);
}

void Game::init() {
    std::cout << "[GAME]--> Load Resources" << std::endl;

    boost::filesystem::path resourcesPath(resourcesConfig["resources"]["path"].as<std::string>());
    boost::filesystem::path modelsPath(resourcesConfig["resources"]["path"].as<std::string>() + resourcesConfig["resources"]["models"]["path"].as<std::string>());
    boost::filesystem::path shadersPath(resourcesConfig["resources"]["path"].as<std::string>() + resourcesConfig["resources"]["shaders"]["path"].as<std::string>());
    boost::filesystem::path fontsPath(resourcesConfig["resources"]["path"].as<std::string>() + resourcesConfig["resources"]["fonts"]["path"].as<std::string>());
    boost::filesystem::path texturesPath(resourcesConfig["resources"]["path"].as<std::string>() + resourcesConfig["resources"]["textures"]["path"].as<std::string>());

    std::vector<std::string> fileExtensions;

    if (boost::filesystem::is_directory(resourcesPath)) {
        if (boost::filesystem::is_directory(modelsPath)) {
            fileExtensions = resourcesConfig["resources"]["models"]["validExtensions"].as<std::vector<std::string>>();
            boost::filesystem::recursive_directory_iterator end_itr;
            // cycle through the directory
            for (boost::filesystem::recursive_directory_iterator i(modelsPath); i != end_itr; ++i) {
                const boost::filesystem::path cp = i->path();
                std::string ext = cp.extension().c_str();
                boost::erase_all(ext, ".");
                if (cp.has_extension() && std::find(fileExtensions.begin(), fileExtensions.end(), ext) != fileExtensions.end()) {
                    // TODO crear lectura de modelos asincrona
                    //auto t1 = std::async(std::launch::async, &ResourceManager::LoadModel, cp.c_str(), cp.stem().c_str());
                    //ResourceManager::LoadModel(cp.c_str(), cp.stem().c_str());


                }
            }
        }
        if (boost::filesystem::is_directory(shadersPath)) {
            boost::filesystem::recursive_directory_iterator end_itr;
            fileExtensions = resourcesConfig["resources"]["shaders"]["validExtensions"].as<std::vector<std::string>>();
            // cycle through the directory
            for (boost::filesystem::recursive_directory_iterator i(modelsPath); i != end_itr; ++i) {
                const boost::filesystem::path cp = i->path();
                std::string ext = cp.extension().c_str();
                boost::erase_all(ext, ".");
                if (cp.has_extension() && std::find(fileExtensions.begin(), fileExtensions.end(), ext) != fileExtensions.end()) {
                    // TODO crear lectura de modelos asincrona
                    //auto t1 = std::async(std::launch::async, &ResourceManager::LoadShader, cp.c_str(), cp.stem().c_str());
                    //ResourceManager::LoadModel(cp.c_str(), cp.stem().c_str());
                }
            }
        }
        if (boost::filesystem::is_directory(fontsPath)) {

        }
        if (boost::filesystem::is_directory(texturesPath)) {

        }
    }

    // Leer los Shaders
    ResourceManager::LoadShader("shaders/basic/basicVertex.glsl", "shaders/basic/basicFragment.glsl", nullptr, "basic");
    ResourceManager::LoadShader("shaders/model/modelVertex.glsl", "shaders/model/modelFragment.glsl", nullptr, "model");
    //ResourceManager::LoadShader("shaders/simpleLight/simpleLightVertex.glsl", "shaders/simpleLight/simpleLightFragment.glsl", nullptr, "model");
    ResourceManager::LoadShader("shaders/light/lightVertex.glsl", "shaders/light/lightFragment.glsl", nullptr, "light");
    ResourceManager::LoadShader("shaders/text/textVertex.glsl", "shaders/text/textFragment.glsl", nullptr, "text");

    // Leer los objetos
    ResourceManager::LoadModel("resources/obj/nanosuit/nanosuit.obj", "nanosuit");
    //ResourceManager::LoadModel("resources/obj/black_dragon/Dragon 2.5_fbx.fbx", "bdragon");
    //ResourceManager::LoadModel("resources/obj/black_dragon/Dragon_Baked_Actions_fbx_7.4_binary(2).fbx", "bdragon");
    ResourceManager::LoadModel("resources/obj/character_running/Character Running.dae", "budy");
    ResourceManager::LoadModel("resources/fbx/Walking.fbx", "walk");
    //ResourceManager::LoadModel("resources/obj/mono/mono.obj", "mono");
    //ResourceManager::LoadModel("resources/obj/Dragon/Dargon_posing.obj", "dragon");
    //ResourceManager::LoadModel("resources/obj/Pterodactyl/Pterodactyl.obj", "petro");
    ResourceManager::LoadModel("resources/obj/esfera.obj", "light");

    // Leer las fuentes
    ResourceManager::LoadFont("resources/fonts/Roboto/Roboto-Regular.ttf", "roboto");

    gameMap = new GameMap();
    renderer = new Renderer();

    std::cout << "[GAME]--> START Game Loop" << std::endl;
    this->gameLoop();
    std::cout << "[GAME]--> END Game Loop" << std::endl;
    ResourceManager::Clear();
}

void Game::gameLoop() {
/* Loop until the user closes the window */

    double lastTime = glfwGetTime();
    nbFrames = 0;

    while (!glfwWindowShouldClose(_window->getWindow()))
    {
        this->renderer->setShader(ResourceManager::GetShader(arrayShader[s % arrayShaderLenght]));

        auto currentFrame = (float) glfwGetTime();
        this->deltaTime = currentFrame - this->lastFrame;
        this->lastFrame = currentFrame;

        nbFrames++;
        if ( currentFrame - lastTime >= 1.0 ){ // If last prinf() was more than 1 sec ago
            // printf and reset timer
            lastnbFrames = nbFrames;
            nbFrames = 0;
            lastTime += 1.0;
        }

        this->processInput();

        /* Update here */
        this->update();

        /* Render here */
        this->render();

        /* Swap front and back buffers */
        glfwSwapBuffers(_window->getWindow());
        /* Poll for and process events */
        glfwPollEvents();
        glfwSwapInterval(windowConfig["window"]["vSync"].as<bool>());
    }
}

void Game::update() {
    if (this->state == GAME_ACTIVE) {
        //this->gameMap->pointLights[0]->setPosition(glm::vec3(5 * sin(glfwGetTime()), this->gameMap->pointLights[0]->getPosition().y, this->gameMap->pointLights[0]->getPosition().z));
        //this->gameMap->spotLights[0]->setPosition(glm::vec3(this->gameMap->spotLights[0]->getPosition().x, this->gameMap->spotLights[0]->getPosition().y,10 + 5 * sin(glfwGetTime())));
        //this->gameMap->dirLights[0]->setPosition(glm::vec3(this->gameMap->dirLights[0]->getPosition().x, this->gameMap->dirLights[0]->getPosition().y, 5 * sin(glfwGetTime())));
        //this->gameMap->objects[0]->rotate(glm::vec3(0, glfwGetTime(), 0));
    } else if (this->state == GAME_MENU) {

    }
}

void Game::render() {

    //if (this->state == GAME_ACTIVE) {
        this->renderer->prepare();

        this->renderer->render(gameMap, _window);

        this->renderer->render(lastnbFrames, _window);
        //sprintf(text, "Shader: %s", arrayShader[s%arrayShaderLenght].c_str());
        //str = text;
        //ResourceManager::GetFont("roboto").Draw(ResourceManager::GetShader("text"), str, 5.0f, _window->getSize().height - 50.0f, 0.5f, glm::vec3(1.0, 0.0, 0.0));

    //} else if (this->state == GAME_MENU) {

    //}
}

void Game::processInput() {

    camera = this->gameMap->getActiveCamera();

    // Salida brusca
    if ((glfwGetKey(_window->getWindow(), GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS || glfwGetKey(_window->getWindow(), GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS ) && glfwGetKey(_window->getWindow(), GLFW_KEY_Q) == GLFW_PRESS) {
        glfwSetWindowShouldClose(_window->getWindow(), true);
    }
    if (this->state == GAME_ACTIVE) {
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_W) == GLFW_PRESS)
            camera->ProcessKeyboard(FORWARD, deltaTime);
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_S) == GLFW_PRESS)
            camera->ProcessKeyboard(BACKWARD, deltaTime);
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_A) == GLFW_PRESS)
            camera->ProcessKeyboard(LEFT, deltaTime);
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_D) == GLFW_PRESS)
            camera->ProcessKeyboard(RIGHT, deltaTime);
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(_window->getWindow(), GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS)
            camera->ProcessKeyboard(DOWN, deltaTime);
        if (glfwGetKey(_window->getWindow(), GLFW_KEY_SPACE) == GLFW_PRESS)
            camera->ProcessKeyboard(UP, deltaTime);
    } else if (Game::state == GAME_MENU) {

    }
}

float Game::getFrameTimeSeconds() {
    return deltaTime;
}

void Game::loadBasicConfiguration() {
    // Load Configuration
    this->windowConfig = YAML::LoadFile("config/windowConfig.yaml");
    this->resourcesConfig = YAML::LoadFile("config/resourcesConfig.yaml");
}

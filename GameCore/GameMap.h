//
// Created by rosende on 13/06/17.
//

#ifndef GAMESTARTER_GAMEMAP_H
#define GAMESTARTER_GAMEMAP_H

#include <glm/glm.hpp>

#include <iostream>
#include <vector>

#include "../Objects/Object.h"
#include "../Objects/Cameras/Camera.h"
#include "../Objects/Lights/Light.h"
#include "../Objects/Lights/DirLight.h"
#include "../Objects/Lights/PointLight.h"
#include "../Objects/Lights/SpotLight.h"
#include "../RenderEngine/Window.h"

#include "ResourceManager.h"

class GameMap {

    void init();
    Shader shader;

public:
    std::vector<Object*> objects;
    std::vector<DirLight*> dirLights;
    std::vector<SpotLight*> spotLights;
    std::vector<PointLight*> pointLights;
    std::vector<Camera*> cameras;

    int activeCamera;

    GameMap();
    ~GameMap();

    Camera* getActiveCamera();
    void setActiveCamera(int n);

    Shader getShader();
    void setShader(Shader shader);

    void render(Window *window);

};


#endif //GAMESTARTER_GAMEMAP_H

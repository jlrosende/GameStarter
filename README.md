# Game Starter
#### Librerias
- [GLFW](glfw.org) ventanas
- [GLEW](glew.sourceforge.net) carga de OpenGL
- [GLM](glm.g-truc.net) calculo de vectores y matrices
- [SOIL](lonesock.net/soil.html) caraga de imagenes y texturas
- [BOOST](boost.org) libreria de uso general
- [YAML-CPP](https://github.com/jbeder/yaml-cpp) archivos de configuracion

#### ¿Que es esto?
Es un proyecto que se quiere terminar como un motor basico para videojuegos, simple de facil uso y muy opimizado.
Por el momento es para aprender como funciona un videojuego a muy bajo nivel.

//
// Created by rosende on 26/02/17.
//

#ifndef GAMESTARTER_WINDOW_H
#define GAMESTARTER_WINDOW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <string>

struct size {
    int height;
    int width;
};

class Window {

    GLFWwindow *_window;
    std::string _title;

public:

    Window(std::string title, int width = 0, int height = 0, std::string icon = "", std::string icon_sm = "");
    ~Window();

    GLFWwindow *getWindow();

    void setSize(size s);
    size getSize();

    void setTitle(std::string title);
    std::string getTitle();

    void setIcons(std::string *images);

    void setMonitor(GLFWmonitor *m);
    GLFWmonitor* getMonitor();

    float getAspectRatio();
};

#endif //GAMESTARTER_WINDOW_H

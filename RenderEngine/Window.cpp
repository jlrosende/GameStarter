//
// Created by rosende on 26/02/17.
//

#include "Window.h"

Window::Window(std::string title, int width, int height, std::string icon, std::string icon_sm) {
    /*
    GLFWimage images[2];
    if (icon == NULL) {
        images[0] = load_icon("my_icon.png");
    } else {
        images[0] = load_icon(icon);
    }

    if (icon_sm == NULL) {
        images[1] = load_icon("my_icon_small.png");
    } else {
        images[1] = load_icon(icon_sm);
    }
    */

    if (width == 0 || height == 0) {
        GLFWmonitor *monitor = glfwGetPrimaryMonitor();
        const GLFWvidmode* mode = glfwGetVideoMode(monitor);
        glfwWindowHint(GLFW_RED_BITS, mode->redBits);
        glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
        glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
        glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

        _window = glfwCreateWindow(mode->width, mode->height, title.c_str(), monitor, NULL);
    } else {
        _window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);
    }

    /* Create a windowed mode window and its OpenGL context */
    if (!_window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    /* Make the window's context current */
    glfwMakeContextCurrent(_window);
    //glfwSetWindowIcon(_window, 2, images);
}

Window::~Window()  {
    glfwDestroyWindow(_window);
}

GLFWwindow* Window::getWindow() {
    return _window;
}

void Window::setSize(size s) {
    glfwSetWindowSize(_window, s.width, s.height);
}

size Window::getSize() {
    size s;
    glfwGetWindowSize(_window, &s.width, &s.height);
    return s;
}

void Window::setTitle(std::string title) {
    glfwSetWindowTitle(_window, title.c_str());
    _title = title;
}

std::string Window::getTitle() {
    return _title;
}

void Window::setIcons(std::string *img) {
    /* TODO Revisar el metodo setIcons
    GLFWimage images[2];
    images[0] = load_icon(img[0]);
    images[1] = load_icon(img[1]);
    glfwSetWindowIcon(_window, 2, images);
    */
}

void Window::setMonitor(GLFWmonitor *m) {
    const GLFWvidmode* mode = glfwGetVideoMode(m);
    glfwSetWindowMonitor(_window, m, 0, 0, mode->width, mode->height, mode->refreshRate);
}

GLFWmonitor* Window::getMonitor() {
    return glfwGetWindowMonitor(_window);
}

float Window::getAspectRatio() {
    size s = this->getSize();
    return s.width/s.height;
}

//
// Created by rosende95 on 27/11/17.
//

#ifndef GAMESTARTER_RENDERER_H
#define GAMESTARTER_RENDERER_H

#include <GL/glew.h>
#include "../Objects/Model.h"
#include "../Objects/Object.h"
#include "../GameCore/GameMap.h"

class Renderer {

    Shader _shader;

public:

    Renderer();

    void setShader(Shader shader);
    Shader getShader();

    void prepare();

    void render(Mesh *mesh);
    void render(Model *model);
    void render(Object *object);
    void render(Light *light);
    void render(DirLight *dirLight, unsigned int nLight);
    void render(PointLight *pointLight, unsigned int nLight);
    void render(SpotLight *spotLight, unsigned int nLight);
    void render(GameMap *gameMap, Window *window);
    void render(int frames, Window *window);

};


#endif //GAMESTARTER_RENDERER_H

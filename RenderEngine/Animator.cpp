//
// Created by rosende95 on 11/12/17.
//

#include "Animator.h"

Animator::Animator(Model entity) : entity(entity), currentAnimation(nullptr), animationTime(0) { }

void Animator::doAnimation(Animation *animation)  {
    this->animationTime = 0;
    this->currentAnimation = animation;
}

void Animator::update() {
    if (currentAnimation == nullptr) {
        return;
    }
    increaseAnimationTime();
    std::map<std::string, glm::mat4> currentPose = calculateCurrentAnimationPose();
    applyPoseToJoints(currentPose, entity.getRootJoint(), glm::mat4());

}

void Animator::increaseAnimationTime() {
    animationTime += Game::getFrameTimeSeconds();
    if (animationTime > currentAnimation->getLength()) {
        this->animationTime = static_cast<float>(fmod(this->animationTime, currentAnimation->getLength()));
    }
}

std::map<std::string, glm::mat4> Animator::calculateCurrentAnimationPose() {
    std::vector<KeyFrame*> frames = getPreviousAndNextFrames();
    float progression = calculateProgression(*frames[0], *frames[1]);
    return interpolatePoses(*frames[0], *frames[1], progression);
}

void Animator::applyPoseToJoints(std::map<std::string, glm::mat4> currentPose, Joint joint, glm::mat4 parentTransform) {
    glm::mat4 currentLocalTransform = currentPose[joint.name];
    glm::mat4 currentTransform = parentTransform * currentLocalTransform;
    for (Joint childJoint : joint.children) {
        applyPoseToJoints(currentPose, childJoint, currentTransform);
    }
    currentTransform *= joint.getInverseBindTransform();
    joint.setAnimationTransform(currentTransform);
}

std::vector<KeyFrame*> Animator::getPreviousAndNextFrames()  {
    std::vector<KeyFrame*> allFrames = currentAnimation->getKeyFrames();
    KeyFrame *previousFrame = allFrames[0];
    KeyFrame *nextFrame = allFrames[0];
    for (unsigned int i = 1; i < allFrames.size(); i++) {
        nextFrame = allFrames[i];
        if (nextFrame->getTimeStamp() > animationTime) {
            break;
        }
        previousFrame = allFrames[i];
    }
    std::vector<KeyFrame*> result;
    result.push_back(previousFrame);
    result.push_back(nextFrame);
    return result;
}

float Animator::calculateProgression(KeyFrame previousFrame, KeyFrame nextFrame) {
    float totalTime = nextFrame.getTimeStamp() - previousFrame.getTimeStamp();
    float currentTime = animationTime - previousFrame.getTimeStamp();
    return currentTime / totalTime;
}

std::map<std::string, glm::mat4> Animator::interpolatePoses(KeyFrame previousFrame, KeyFrame nextFrame, float progression) {
    std::map<std::string, glm::mat4> currentPose;
    for(auto it = previousFrame.getJointKeyFrames().begin(); it != previousFrame.getJointKeyFrames().end(); ++it) {
        JointTransform previousTransform = previousFrame.getJointKeyFrames().find(it->first)->second;
        JointTransform nextTransform = nextFrame.getJointKeyFrames().find(it->first)->second;
        JointTransform currentTransform = JointTransform::interpolate(previousTransform, nextTransform, progression);
        currentPose[it->first] = currentTransform.getLocalTransform();
    }
    return currentPose;
}




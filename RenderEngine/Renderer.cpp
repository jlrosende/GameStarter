//
// Created by rosende95 on 27/11/17.
//

#include "Renderer.h"

Renderer::Renderer() {
    glEnable(GL_DEPTH_TEST);
}

void Renderer::prepare() {
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::setShader(Shader shader) {
    this->_shader = shader;
}

Shader Renderer::getShader() {
    return this->_shader;
}

void Renderer::render(Mesh *mesh) {
    unsigned int diffuseNr  = 1;
    unsigned int specularNr = 1;
    unsigned int normalNr   = 1;
    unsigned int heightNr   = 1;
    if (mesh->textures.size() > 0) {
        glUniform1i(glGetUniformLocation(_shader.ID, "HadTextures"), 1);
    } else {
        glUniform1i(glGetUniformLocation(_shader.ID, "HadTextures"), 0);
    }
    for(unsigned int i = 0; i < mesh->textures.size(); i++) {
        glActiveTexture(GL_TEXTURE0 + i); // activar cada textura antes de unirlas
        // asigno un nombre para cada textura
        std::stringstream ss;
        std::string number;
        std::string name = mesh->textures[i].type.data();
        if (name == "texture_diffuse") {
            ss << diffuseNr++;
        } else if (name == "texture_specular") {
            ss << specularNr++;
        } else if (name == "texture_normal") {
            ss << normalNr++;
        } else if (name == "texture_height") {
            ss << heightNr++;
        }
        number = ss.str();
        // paso las texturas al shader
        //std::cout << ("material." + name + number) << std::endl;
        glUniform1i(glGetUniformLocation(_shader.ID, ("material." + name + number).c_str()), i);
        // Linco las texturas segun su id
        glBindTexture(GL_TEXTURE_2D, mesh->textures[i].id);
    }

    for (unsigned int i = 0; i < mesh->materials.size(); i++) {
        _shader.SetVector3f("material.diffuse_color", mesh->materials[i].diffuse);
        _shader.SetVector3f("material.specular_color", mesh->materials[i].specular);
        _shader.SetVector3f("material.ambient_color", mesh->materials[i].ambient);
        _shader.SetFloat("material.opacity", mesh->materials[i].opacity);
        _shader.SetFloat("material.shininess", mesh->materials[i].shininess);
    }

    // pintar maya
    glBindVertexArray(mesh->VAO);
    glDrawElements(GL_TRIANGLES, mesh->indices.size(), GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // always good practice to set everything back to defaults once configured.
    glActiveTexture(GL_TEXTURE0);

}

void Renderer::render(Model *model) {
    //std::cout << "\tModel: " << &model << std::endl;
    for (unsigned int i = 0; i < model->meshes.size(); i++)
        this->render(&model->meshes[i]);
        //model->meshes[i].render(this->_shader);
}

void Renderer::render(Object *object) {
    //std::cout << "Object: " << object << std::endl;
    //std::cout << "Shader: " << _shader.name << std::endl;
    _shader.Use();
    _shader.SetMatrix4("model", object->getTransformationMatrix());
    //_shader.SetFloat("material.shininess", 32);
    this->render(object->getModel());

}

void Renderer::render(GameMap *gameMap, Window *window) {
/*
    // Configurar los shaders
    glm::mat4 projection = glm::perspective(glm::radians(gameMap->getActiveCamera()->Zoom), window->getAspectRatio(), 0.1f, 100.0f);
    glm::mat4 view = gameMap->getActiveCamera()->GetViewMatrix();

    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("projection", projection);
    ResourceManager::GetShader("light").SetMatrix4("view", view);

    if (!gameMap->dirLights.empty()) {
        for (unsigned int i = 0; i < gameMap->dirLights.size(); i++) {
            this->render(gameMap->dirLights[i], i);
        }
    }

    if (!gameMap->pointLights.empty()) {
        for (unsigned int i = 0; i < gameMap->pointLights.size(); i++) {
            this->render(gameMap->pointLights[i], i);
        }
    }

    if (!gameMap->spotLights.empty()) {
        for (unsigned int i = 0; i < gameMap->spotLights.size(); i++) {
            this->render(gameMap->spotLights[i], i);
        }
    }

    _shader.Use();
    //ResourceManager::GetShader("model").SetVector3f("lightPos", lights[0]->getPosition());
    _shader.SetMatrix4("projection", projection);
    _shader.SetMatrix4("view", view);

    _shader.SetInteger("numDirLights", (GLint) gameMap->dirLights.size());
    _shader.SetInteger("numPointLights", (GLint) gameMap->pointLights.size());
    _shader.SetInteger("numSpotLights", (GLint) gameMap->spotLights.size());

    _shader.SetVector3f("viewPos", gameMap->getActiveCamera()->getPosition());

    for (unsigned int i = 0; i < gameMap->objects.size(); ++i) {
        this->render(gameMap->objects[i]);
    }
*/

    gameMap->setShader(_shader);
    gameMap->render(window);
}

void Renderer::render(Light *light) {
    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("model", light->getTransformationMatrix());
    ResourceManager::GetShader("light").SetVector3f("lightColor", (light->getColor()));
    this->render(light->getModel());
    _shader.Use();
    _shader.SetVector3f("lightPos", light->getPosition());
}

void Renderer::render(DirLight *dirLight, unsigned int nLight) {
    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("model", dirLight->getTransformationMatrix());
    ResourceManager::GetShader("light").SetVector3f("lightColor", (dirLight->getColor()));
    this->render(dirLight->getModel());

    std::string s("dirLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    _shader.Use();
    _shader.SetVector3f("lightPos", dirLight->getPosition());
    _shader.SetVector3f((s + "position").c_str(), dirLight->getPosition());
    _shader.SetVector3f((s + "direction").c_str(), dirLight->getDirection());
    _shader.SetVector3f((s + "ambient").c_str(), dirLight->getAmbient());
    _shader.SetVector3f((s + "diffuse").c_str(), dirLight->getDiffuse());
    _shader.SetVector3f((s + "specular").c_str(), dirLight->getSpecular());
}

void Renderer::render(PointLight *pointLight, unsigned int nLight) {
    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("model", pointLight->getTransformationMatrix());
    ResourceManager::GetShader("light").SetVector3f("lightColor", (pointLight->getColor()));
    this->render(pointLight->getModel());

    std::string s("pointLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    _shader.Use();
    _shader.SetVector3f("lightPos", pointLight->getPosition());
    _shader.SetVector3f((s + "position").c_str(), pointLight->getPosition());
    _shader.SetVector3f((s + "ambient").c_str(), pointLight->getAmbient());
    _shader.SetVector3f((s + "diffuse").c_str(), pointLight->getDiffuse());
    _shader.SetVector3f((s + "specular").c_str(), pointLight->getSpecular());
    _shader.SetFloat((s + "constant").c_str(), pointLight->getConstant());
    _shader.SetFloat((s + "linear").c_str(), pointLight->getLinear());
    _shader.SetFloat((s + "quadratic").c_str(), pointLight->getQuadratic());
}

void Renderer::render(SpotLight *spotLight, unsigned int nLight) {
    ResourceManager::GetShader("light").Use();
    ResourceManager::GetShader("light").SetMatrix4("model", spotLight->getTransformationMatrix());
    ResourceManager::GetShader("light").SetVector3f("lightColor", (spotLight->getColor()));
    this->render(spotLight->getModel());

    std::string s("spotLights[");
    char number[3];
    sprintf(number, "%d" ,nLight);
    s = s + number + "].";
    _shader.Use();
    _shader.SetVector3f("lightPos", spotLight->getPosition());
    _shader.SetVector3f((s + "position").c_str(), spotLight->getPosition());
    _shader.SetVector3f((s + "direction").c_str(), spotLight->getDirection());
    _shader.SetVector3f((s + "ambient").c_str(), spotLight->getAmbient());
    _shader.SetVector3f((s + "diffuse").c_str(), spotLight->getDiffuse());
    _shader.SetVector3f((s + "specular").c_str(), spotLight->getSpecular());
    _shader.SetFloat((s + "constant").c_str(), spotLight->getConstant());
    _shader.SetFloat((s + "linear").c_str(), spotLight->getLinear());
    _shader.SetFloat((s + "quadratic").c_str(), spotLight->getQuadratic());
    _shader.SetFloat((s + "cutOff").c_str(), spotLight->getCutOff());
    _shader.SetFloat((s + "outerCutOff").c_str(), spotLight->getOuterCutOff());
}

void Renderer::render(int frames, Window *window) {
    ResourceManager::GetShader("text").Use();
    glm::mat4 proj = glm::ortho(0.0f, (float)window->getSize().width, 0.0f, (float)window->getSize().height);
    ResourceManager::GetShader("text").SetMatrix4("projection", proj);
    char text[50];
    sprintf(text, "fps: %d", frames);
    std::string str(text);
    ResourceManager::GetFont("roboto").Draw(ResourceManager::GetShader("text"), str, 5.0f, window->getSize().height - 25.0f, 0.5f, glm::vec3(1.0, 0.0, 0.0));
}







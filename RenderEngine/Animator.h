//
// Created by rosende95 on 11/12/17.
//

#ifndef GAMESTARTER_ANIMATOR_H
#define GAMESTARTER_ANIMATOR_H


#include "../Objects/Model.h"
#include "../Objects/Animation.h"
#include "../GameCore/Game.h"
#include "../Objects/Joint.h"

class Animator {
    Model entity;
    Animation *currentAnimation;
    float animationTime;

public:
    explicit Animator(Model entity);

    void doAnimation(Animation *animation);

    void update();

    void increaseAnimationTime();

    std::map<std::string, glm::mat4> calculateCurrentAnimationPose();

    void applyPoseToJoints( std::map<std::string, glm::mat4> currentPose, Joint joint, glm::mat4 parentTransform);

    std::vector<KeyFrame*> getPreviousAndNextFrames();

    float calculateProgression(KeyFrame previousFrame, KeyFrame nextFrame);

    std::map<std::string, glm::mat4> interpolatePoses(KeyFrame previousFrame, KeyFrame nextFrame, float progression);

};


#endif //GAMESTARTER_ANIMATOR_H
